__author__ = 'luisfernando'

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "ValidaRFC",
    version = "0.0.1.2",
    author = "Luis Fernando Barrera",
    author_email = "luisfernando@informind.com",
    description = ("Python Tools to validate and generate Mexican RFC (Registro Federal de Contribuyentes) Tax ID Code"
                    " and CURP (Clave Unica de Registro de Poblacion)"),
    license = "MIT",
    keywords = "mexico tax-code rfc curp personal-id",
    url = "http://code.informind.com/validarfc/",
    packages = ['validarfc'],
    long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
    ],
)
