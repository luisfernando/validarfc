# -*- coding: utf-8 -*-
__author__ = 'luisfernando'


from validarfc import RFCValidator
from validarfc import RFCGeneratorFisicas
import unittest
import datetime
import os
import pickle

class test_RFCValidator(unittest.TestCase):

    def test_ValidRFC(self):
        rfc = [
            ('MANO610814JL5', True),
            ('MME941130K54', True),
            ('BACL891217NJ8', False),
            ('NIR6812205X9', True),
            ('BNM840515VB1', True),
        ]

        for elem, result in rfc:
            #print elem, result
            self.assertEqual(RFCValidator(elem).validate(), result)


class test_RFCPersonasFisicas(unittest.TestCase):
    def test_generaLetras(self):
        tests = [
            ('Juan', 'Barrios', u'Fernández', 'BAFJ'),
            ('Eva', 'Iriarte', u'Méndez', 'IIME'),
            ('Manuel', u'Chávez', u'González', 'CAGM'),
            ('Felipe', 'Camargo', 'Lleras', 'CALF'),
            ('Charles', 'Kennedy', 'Truman', 'KETC'),
            ('Alvaro', 'De la O', 'Lozano', 'OLAL'),
            ('Ernesto', 'Ek', 'Rivera', 'ERER'),
            ('Julio', 'Ek', '', 'EKJU'),
            ('Julio', 'Ek', None, 'EKJU'),
            ('Luis', u'Bárcenas', '', 'BALU'),
            ('Dolores', u'San Martín', u'Dávalos', 'SADD'),
            ('Mario', u'Sánchez de la Barquera', u'Gómez', 'SAGM'),
            ('Antonio', u'Jiménez', u'Ponce de León', 'JIPA'),
            (u'Luz María', u'Fernández', u'Juárez', 'FEJL'),
            (u'José Antonio', 'Camargo', u'Hernández', 'CAHA'),
            (u'María de Guadalupe', u'Hernández', u'von Räutlingen', 'HERG'),
            (u'María Luisa', u'Ramírez', u'Sánchez', 'RASL'),
            (u'Ernesto', u'Martínez', u'Mejía', 'MAMX'),
            (u'Fernando', u'Ñemez', 'Ñoz', u'ÑEÑF'),
            (u'泽东', u'毛', '', 'MAZE'), #Mao Zedong
            (u'中山', u'孙', '', 'SUZH'), #Sun Zhongshan
            (u'中山', u'孙', None, 'SUZH')
        ]

        for elem in tests:
            r = RFCGeneratorFisicas(nombre=elem[0], paterno=elem[1], materno=elem[2], fecha=datetime.date(2000, 1, 1))
            self.assertEqual(r.generate_letters(), elem[3])
            print r.generate_letters(), elem[3]
            print r.rfc

class test_RNIE(unittest.TestCase):

    def setUp(self):
        self.PICKLE_FILE = 'p.pickle'
        try:
            self.r = self.loadPickle()
        except EnvironmentError:
            self.skipTest('Cannot load Pickle')

    def loadPickle(self):
        PICKLE = os.path.join(os.path.dirname(__file__), self.PICKLE_FILE)
        print PICKLE
        if os.path.exists(PICKLE):
            with open(PICKLE) as f:
                r = pickle.load(f)
                return r
        else:
            raise EnvironmentError


    def test_verifyValidity(self):
        rfc = [c[0].decode('latin1') for c in self.r]
        for index, elem in enumerate(rfc):
            try:
                r = RFCValidator(elem)
                if not r.is_valid():
                    print index, r.is_moral(), elem, r.validators()
            except Exception as e:
                print index, elem, e